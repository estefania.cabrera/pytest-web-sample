@LogIn
Feature: Log In - Log Out

  Background:
    Given The user is placed in the practice page


  Scenario Outline: Log-in with valid username and password
    When Click on My Account Menu
    And Enter the username <username>
    And Enter the password <password> in the text box
    And Click on login button
    Then User must successfully login to the web page

    Examples:
      | username          | password    |
      | estefania.cabrera | Crowdar2022 |

  @LogInError
  Scenario Outline: Log-in Errors
    When Click on My Account Menu
    And Enter the username <username>
    And Enter the password <password> in the text box
    And Click on login button
    Then Proper error <messageError> must be displayed and prompt to enter login again

    Examples:
      | username                            | password    | messageError                                              |
      |                                     | Crowdar2022 | Error: Username is required.                              |
      | estefania@crowdaronline.com         | Crowdar2022 | Error: A user could not be found with this email address. |
      | estefania.cabrera@crowdaronline.com |             | Error: Password is required.                              |

  @LogOut
  Scenario Outline: My Accounts Log out
      When Click on My Account Menu
      And Enter the username <username>
      And Enter the password <password> in the text box
      And Click on login button
      And Click on Logout button
      Then User successfully comes out from the site and returns to login page
      Examples:
        | username          | password    |
        | estefania.cabrera | Crowdar2022 |

