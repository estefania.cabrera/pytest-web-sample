from pages import LoginPage as page
from core.steps.BaseSteps import BaseStep
from core.assertion.Assertion import Assertion

class LoginSteps(BaseStep):
    def clickMyAccount(self):
        elem = page.MY_ACCOUNT_BUTTON()
        elem.click(elem)
        return self

    def setUsernameInput(self, username):
        elem = page.USERNAME_INPUT()
        elem.setText(username)
        Assertion.assertTrue('Username is not displayed', page.USERNAME_INPUT().isDisplayed())
        return self

    def setPasswordInput(self, password):
        elem = page.PASSWORD_INPUT()
        elem.setText(password)
        Assertion.assertTrue('Password is not displayed', page.PASSWORD_INPUT().isDisplayed())
        return self

    def clickLogin(self):
        elem = page.LOGIN_BUTTON()
        elem.click(elem)
        return self

    def validateSignOut(self):
        Assertion.assertTrue('Sign out button is not displayed', page.SIGN_OUT_BUTTON().isDisplayed())
        return self

    def validateError(self, messageError):
        elem = page.LOGIN_ERROR_DESCRIPTION()
        Assertion.assertEquals('El mensaje no es equivalente', messageError, elem.getText())

    def clickLogOut(self):
        elem = page.LOG_OUT_BUTTON()
        elem.click(elem)
        return self

    def validateLogOut(self):
        Assertion.assertTrue('Log out button is not displayed', page.LOGIN_HEATHER().isDisplayed())
        return self


