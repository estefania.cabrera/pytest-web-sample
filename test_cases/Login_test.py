from pytest_bdd import then, scenarios, given, when
from steps.LoginSteps import LoginSteps as LoginSteps
from core.steps.BaseSteps import BaseStep as BaseStep
#from steps.GoogleSearchSteps import GoogleSearchSteps as googleSearch

scenarios('../features/Login.feature',
          )

@given('The user is placed in the practice page')
def validatePage():
    LoginSteps().getTitle()

@when('Click on My Account Menu')
def clickMyAccount():
    LoginSteps().clickMyAccount()

@when('Enter the username <username>')
def inputUsername(username):
    LoginSteps().setUsernameInput(username)

@when('Enter the password <password> in the text box')
def inputPassword(password):
    LoginSteps().setPasswordInput(password)

@when('Click on login button')
def clickLogin():
    LoginSteps().clickLogin()

@then('User must successfully login to the web page')
def validateSignOut():
    LoginSteps().validateSignOut()

@then('Proper error <messageError> must be displayed and prompt to enter login again')
def validateError(messageError):
    LoginSteps().validateError(messageError)

@when('Click on Logout button')
def clickMyAccount():
    LoginSteps().clickLogOut()

@then('User successfully comes out from the site and returns to login page')
def logOutValidation():
    LoginSteps().validateLogOut()

def teardown():
    LoginSteps().closeBrowser()
