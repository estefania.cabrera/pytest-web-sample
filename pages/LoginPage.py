from core.ui.WebUIElement import WebUIElement as UIElement
from core.ui.By import By


def MY_ACCOUNT_BUTTON():
    return UIElement(By.XPATH, "//*[@id='menu-item-50']/a")

def USERNAME_INPUT():
    return UIElement(By.ID, "username")

def PASSWORD_INPUT():
    return UIElement(By.ID, "password")

def LOGIN_BUTTON():
    return UIElement(By.XPATH, "//*[@id='customer_login']/div[1]/form/p[3]/input[3]")

def SIGN_OUT_BUTTON():
    return UIElement(By.XPATH, "//*[@id='page-36']/div/div[1]/div/p[1]/a")

def LOGIN_ERROR_DESCRIPTION():
    return UIElement(By.XPATH, "//*[@id='page-36']/div/div[1]/ul/li")

def LOG_OUT_BUTTON():
    return UIElement(By.XPATH, "//*/div[1]/nav/ul/li[6]/a")

def LOGIN_HEATHER():
    return UIElement(By.XPATH, "//*/div[1]/h2")
